__author__ = 'oliver'

###START-CONF
##{
##"object_name": "plotting",
##"object_poi": "my-plot-1234",
##"auto-load" : true,
##"parameters": [ {
##                  "name": "predictions",
##                  "description": "all predictions",
##                  "required": true,
##                  "type": "String",
##                  "state" : "PREDICTED"
##               }],
##"return": [
##
##          ] }
##END-CONF

import nltk, json
import datetime, numpy, time
from dateutil import parser
import matplotlib.pyplot as plt
import matplotlib
import sys
from pumpkin import *

matplotlib.use('Agg')
class TTweet():
    def __init__(self, date, text, prediction):
        self.date = parser.parse(date)
        self.text = text
        self.prediction = prediction
        self.serialized = {}
    def serialize(self):
        self.serialized = {
            'date': str(self.date),
            'text': self.text,
            'prediction': self.prediction
        }
        return self.serialized

class plotting(PmkSeed.Seed):
    def __init__(self, context, poi=None):
        PmkSeed.Seed.__init__(self, context,poi)
        pass
    def run(self, pkt, predictions):
        """ Data is transformed at intermediate points on its way
        to a destination. In this case we are simply adding
        "hello" to a name to form a greeting. This will be
        dispatched and received by a collector.
        """
     

        data = json.loads(predictions[0])
        allTweets = []
        for t in data:
            allTweets.append(TTweet(t['date'], t['text'], t['prediction']))
        with open('tweets_classified.json', 'wr') as outfile:
            outfile.write(json.dumps([t.serialize() for t in allTweets]))
        print data
        # According to assignment specs we should do this separately but what would be the point of that..
        """
        dates = []
        predictions = []
        t = 0
        while(t<len(allTweets)):
            subSet = allTweets[t:len(allTweets)/5+t]
            print len(subSet)
            this_date = 0
            this_pred = 0
            for tweet in subSet:
                this_pred += 1 if allTweets[t].prediction == 'pos' else 0
                this_date += time.mktime(allTweets[t].date.timetuple())
            this_pred = 1.0*this_pred/1.0*len(subSet)
            this_date = this_date/len(subSet)
            dates.append(str(this_date))
            predictions.append(this_pred)
            t += len(subSet)
        plt.plot(dates, predictions)
        plt.savefig('predictions.png', bbox_inches='tight')
        print "Saved prediction plot"
        """
        pass













