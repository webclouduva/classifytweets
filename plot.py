import nltk, json
import datetime, numpy, time
from dateutil import parser
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.dates as mdates
import sys
import numpy as np
class TTweet():
    def __init__(self, date, text, prediction):
        self.date = parser.parse(date)
        self.text = text
        self.prediction = prediction
        self.serialized = {}
    def serialize(self):
        self.serialized = {
            'date': str(self.date),
            'text': self.text,
            'prediction': self.prediction
        }
        return self.serialized

with open('tweets_classified.json', 'rb') as infile:
    data = infile.readlines()
allTweets = [TTweet(t['date'], t['text'], t['prediction']) for t in json.loads(data[0])]
pairs = sorted([[d.date, d.prediction] for d in allTweets], key=lambda t: t[0])
dates = []
predictions = []
t = 0
while(t<len(pairs)):
    subSet = pairs[t:len(allTweets)/10+t]
    this_date = 0
    this_pred = 0
    for s in subSet:
        this_pred += 1 if s[1] == 'pos' else 0
        this_date += time.mktime(s[0].timetuple())
    this_pred = (1.0*this_pred)/(1.0*len(subSet))
    this_date = this_date/len(subSet)
    dates.append(datetime.datetime.fromtimestamp(this_date))
    predictions.append(this_pred)
    t += len(subSet)
dates = np.asarray(dates)
predictions = np.asarray(predictions)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_xticks(dates)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
ax.plot_date(dates, predictions, ls='-', marker='o')
ax.grid(True)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
#fig.show()
fig.savefig('predictions.png', bbox_inches='tight')